﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletInstantiation : MonoBehaviour
{
    public GameObject bullet;
    public float fireRate = 5.0f;
    private float timeUntilNextEvent;
    // Start is called before the first frame update
    void Start()
    {
        timeUntilNextEvent = fireRate; // Begins the fire Rate timer
    }

    // Update is called once per frame
    void Update()
    {
        timeUntilNextEvent -= Time.deltaTime; // decerases the timer for fire rate
        //input to fire
        if (timeUntilNextEvent <= 0)
        {
            Debug.Log("ready to fire");
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Vector3 offset = transform.rotation * new Vector3(0, 0, 0.5f);//alligns bullet to face direction of barrell
                GameObject newOject = Instantiate(bullet, GetComponent<Transform>().position, GetComponent<Transform>().rotation); // creates bullet
                Physics.IgnoreCollision(bullet.GetComponent<Collider>(), GetComponent<Collider>());
                timeUntilNextEvent = fireRate; // resets the timer
            }
        }
    }
}

         
