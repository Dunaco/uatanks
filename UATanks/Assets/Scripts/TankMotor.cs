﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMotor : MonoBehaviour
{
    private CharacterController characterController;
    public Transform tf;
    public float NoiseMaker = 5.0f;
    public void Awake()
    {
        tf = gameObject.GetComponent<Transform>();
    }
    // Start is called before the first frame update
    void Start()
    {
        characterController = gameObject.GetComponent<CharacterController>();
    }
    // These lines of code will allow the player's tank to move forward.
    public void Move( float speed )
    {
        Vector3 sVector; // Contains the data for speed
        sVector = transform.forward * speed; // Alligns the vector with the way the player is facing and applies the speed value.
        characterController.SimpleMove(sVector);
    }
    // allows rotation
    public void Rotate( float speed)
    {
        Vector3 rotateVector; 
        rotateVector = Vector3.up * speed * Time.deltaTime; // Allows rotation to be based off of speed and rotates per second.
        transform.Rotate(rotateVector, Space.Self); // Allows rotation in local space.
    }

    public bool RotateTowards(Vector3 target, float speed)
    {
        Vector3 vectorToTarget;
        //find difference betweent waypoint and tank
        vectorToTarget = target - tf.position;
        // find our rotation needed to face waypoint
        Quaternion targetRotation = Quaternion.LookRotation(vectorToTarget);
        // if our rotation returned with 0 or not needed, proceed to move forward
        if (targetRotation == tf.rotation)
        {
            return false;
        }
        // change rotation so we rotate to target in a smooth manner.
        tf.rotation = Quaternion.RotateTowards(tf.rotation, targetRotation,speed * Time.deltaTime);
        return true;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
