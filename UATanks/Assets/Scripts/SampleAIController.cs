﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleAIController : MonoBehaviour
{
    public Transform[] waypoints;
    public TankMotor motor;
    public TankData data;
    private int currentWaypoint = 0;
    public float closeEnough = 1.0f;
    public Transform tf;
    public enum LoopType { Stop, Loop, PingPong};
    public LoopType looptype;
    private bool isPatrolFoward = true;
    public void Awake()
    {
        tf = gameObject.GetComponent<Transform>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (motor.RotateTowards(waypoints[currentWaypoint].position, -data.turnSpeed))
        {
            //do nothing
        }
        else
        {
            //go foward
            motor.Move(data.moveSpeed);
        }
        //if near waypoint
        if (Vector3.SqrMagnitude(waypoints[currentWaypoint].position - tf.position) < (closeEnough * closeEnough)) {
            if (looptype == LoopType.Stop)
            {
                if (currentWaypoint < waypoints.Length - 1)
                {
                    //go to next waypoint
                    currentWaypoint++;
                }
            }
            else if(looptype == LoopType.Loop)
            {
                if (currentWaypoint < waypoints.Length-1)
                {
                    currentWaypoint++;
                }
                else
                {
                    currentWaypoint = 0;
                }
            }
            else if (looptype == LoopType.PingPong)
            {
                if (isPatrolFoward)
                {
                    //proceed foward
                    if (currentWaypoint < waypoints.Length - 1)
                    {
                        currentWaypoint++;
                    }
                    else
                    {
                        //turn around and go back
                        isPatrolFoward = false;
                        currentWaypoint--;
                    }
                }
                else
                {
                    //follow waypoints backwards
                    if (currentWaypoint > 0)
                    {
                        currentWaypoint--;
                    }
                    else
                    {
                        currentWaypoint++;
                    }
                }
            }
        }
    }
}
