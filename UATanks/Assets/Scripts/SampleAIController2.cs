﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleAIController2 : MonoBehaviour
{
    public enum AttackMode { Chase, Flee}
    public AttackMode attackMode;
    public Transform ptf;
    public TankData data;
    public TankMotor motor;
    public float fleeDistance = 1.0f;
    private Transform tf;


    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (attackMode == AttackMode.Chase)
        {
            //rotate towards target
            motor.RotateTowards(ptf.position, data.turnSpeed);
            //Move forward
            motor.Move(data.moveSpeed);
        }
        else if (attackMode == AttackMode.Flee)
        {
            //Calculates distance to player
            Vector3 vectorToTarget = ptf.position - tf.position;
            //reverses the vector to look away from target.
            Vector3 vectorAwayFromTarget = -1 * vectorToTarget;
            //Normalizes the vector
            vectorAwayFromTarget.Normalize();
            //gives fleeing distance
            vectorAwayFromTarget *= fleeDistance;

            //Moves towards fleeing Vector to get away from player
            Vector3 fleePosition = vectorAwayFromTarget + tf.position;
            motor.RotateTowards(fleePosition, data.turnSpeed);
            motor.Move(data.moveSpeed);
        }
    }
}
