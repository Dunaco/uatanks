﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISensoryScript : MonoBehaviour
{
    // data
    public float sightDistance = 10; // How far they can see
    public float lineOfSight = 60; // View angle (Field of View)
    public float hearingDistance = 1.0f; // How well they can hear. If this is 1.0, they hear "normally", otherwise, this is deafness/superhearing
    private float playerDistance_;
    private float targetNoiseMaker;
    public GameObject target;
    
    // Components
    private Transform tankTF;
    // Use this for initialization
    void Start()
    {
        tankTF = GetComponent<Transform>();
    }


    public bool CanHear(GameObject target)
    {
        // If the target doesn't have a noisemaker, we can't hear them!
        targetNoiseMaker = target.GetComponent<TankMotor>().NoiseMaker;
        if (targetNoiseMaker <= 0)
        {
            return false;
        }

        // If they do, check the distance -- if it is <= (noise volume * hearingDistance), then we can hear them!
        Transform targettankTF = target.GetComponent<Transform>();
        if (Vector3.Distance(targettankTF.position, tankTF.position) <= targetNoiseMaker)
        {
            return true;
        }

        // Otherwise, we can't hear them
        return false;
    }

    public bool CanSee(GameObject target)
    {
        // If they do not have a collider, they are invisible
        Transform targetController = target.GetComponent<Transform>();

        // If they are outside the view angle, we cannot see them
        // To check, we need the vector to our target, and compare that angle to our forward vector
        Vector3 playerVector = targetController.position - tankTF.position;
        playerVector.Normalize();

        playerDistance_ = Vector3.Distance(targetController.position, tankTF.position);

        if (Vector3.Angle(tankTF.forward, playerVector) <= lineOfSight)
        {
            int playerMask = 1 << 10;
            int layerMask = (1 << playerMask | ~(1 << playerMask));
            RaycastHit hasHit;

            // If they are in our field-of-view (thus we could get here), 
            //     raycast to make sure nothing is blocking our view

            if (Physics.Raycast(tankTF.position, playerVector, out hasHit, sightDistance, layerMask))
            {
                // If our raycast hit the player first, then we can see them
                if (hasHit.collider.CompareTag("Player1Body"))
                {
                    Debug.Log(gameObject.name + " play Hit " + hasHit.collider.name);
                    Debug.DrawLine(tankTF.position, tankTF.position + playerVector * sightDistance, Color.green);
                    return true;
                }
            }
        }
        // otherwise, if we hit something else or nothing we failed to see them
        Debug.Log(gameObject.name + " Hit Nothing");
        return false;
    }
}