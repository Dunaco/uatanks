﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehavior : MonoBehaviour
{
    Transform tf;
    public float bulletSpeed = 1.0f;
    public float damage = 1f;

    private void OnTriggerEnter(Collider other)
    {
        other.gameObject.GetComponent<TankBehavior>().takeDamage(damage); //calls the function for damage, and sends the damage variable.
        Destroy(gameObject);
        GameManager.instance.ScoreAdd();
    }
    // Start is called before the first frame update

    void Start()
    {
        tf = GetComponent<Transform>(); // gathers transform component
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = transform.position;

        Vector3 velocity = new Vector3(0, 0, bulletSpeed * Time.deltaTime); //bullets movement

        pos += transform.rotation * velocity;
        transform.position = pos;
        Destroy(gameObject, 5); //how long bullet remains in game

    }
}