﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankData : MonoBehaviour
{
    public float moveSpeed = 3; //m/sec
    public float turnSpeed = 180; //Degree/sec
    public float maxHealth = 2f; // maximum HP
    public float curHealth = 0f; // current health variable


    // Start is called before the first frame update
    void Start()
    {
        curHealth = maxHealth;
    }

    public void takeDamage(float damage)
    {
        curHealth -= damage; // applies the damage amount from the bullet.
    }

    // Update is called once per frame
    void Update()
    {
        if (curHealth <= 0)
        {
            Die(); //call the funcion to destroy the tank
        }
    }

    void Die()
    {
        Destroy(gameObject);
        Debug.Log("Tank Destroyed");

    }
}
