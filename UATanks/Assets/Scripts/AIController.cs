﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{
    #region variables
    private AISensoryScript sensing; //holds sensory variables from sensoryscript
    #region transforms and vectors
    public Transform[] waypointSystem; //keeps a list of available waypoints
    public Transform playerCharacter_; //stores either player 1 or 2 transforms if it's not null
    private Transform tankTransform_; //stores the transform of the tank
    private Vector3 fleePosition_; //stores the position to flee to if it's a fleeing tank
    private Vector3 vectorFlee_; //stores the vector of fleeing
    private Vector3 originPoint; //stores origin point to head back to (if idle or lookout state that is)
    #endregion

    #region bools
    private bool enemyHasFired; //checks if enemy has fired or not
    private bool endOfPatrol_; //checks if AI reached the end of their patrol before either looping or retracing
    private bool searchTimeStarted_ = false; //checks if timer has started so it doesn't keep starting and making the timer absurdly fast
    #endregion

    #region floats and ints
    public float howClose = 1.0f; //how close to their objective the AI is before they move to next
    private float fleeDistance = 1.0f; //just a standard flee distance to make the AI run from player
    public float timeBetweenFire; //time to set for designers to limit how fast the AI can shoot
    public float searchTimeAllowed; //designers set a predetermined time for the AI to continue tracking the player
    private float fireTimer_; //keeps track of how long it's been since a shot was fired
    private float searchTimer_; //keeps track of how long it has been since AI last sight of player
    private int currentWaypoint_ = 0; //keeps track of current waypoint that AI is on
    #endregion

    #region enums for states
    public enum TypeofEnemy { Idle, Looping, Retracing, Lookout, Flee, sawPlayer } //might make Flee and sawPlayer their own enum (probably better that way)
    public TypeofEnemy EnemyType; //allows designer to choose initial state of AI
    public TypeofEnemy SwitchTo; //will keep track of enemy switch to jump to by designer (might just remove and make random... or make a separate case)
    private TypeofEnemy originalState_; //stores the original state value for enemies
    #endregion

    #region gameobjects and misc data
    private TankMotor motor_; //stores motor data for movement
    private TankData speedInfo_; //stores different speed variables for tank movement
    private GameObject ammunition_; //stores ammunition for when tank fires
    #endregion

    #endregion

    // Use this for initialization
    void Awake ()
    {
        motor_ = gameObject.GetComponent<TankMotor>();
        speedInfo_ = gameObject.GetComponent<TankData>();;
        tankTransform_ = gameObject.GetComponent<Transform>();
        originalState_ = EnemyType;
        sensing = GetComponent<AISensoryScript>();
        originPoint = gameObject.GetComponent<Transform>().position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //ShellFiring(); //firing at player when it sees player
        

        switch (EnemyType)
        {
            case TypeofEnemy.Idle:
                {
                    Idle();
                    if (sensing.CanSee(GameManager.instance.player1))
                    {
                        playerCharacter_ = GameManager.instance.player1.transform;
                        EnemyType = SwitchTo;
                    }
                    
                    if (sensing.CanHear(GameManager.instance.player1.gameObject))
                    {
                        EnemyType = TypeofEnemy.sawPlayer;
                    }
                    //check transitions
                    //if hearSomething
                    //investigatesound
                    return;
                }

                
            case TypeofEnemy.Looping:
                {
                    TurningAndMovingAI();
                    Looping();
                    if (sensing.CanSee(GameManager.instance.player1))
                    {
                        playerCharacter_ = GameManager.instance.player1.transform;
                        EnemyType = SwitchTo;
                    }
                    if (sensing.CanHear(GameManager.instance.player1.gameObject))
                    {
                        EnemyType = TypeofEnemy.sawPlayer;
                    }
                    //check transitions
                    //if hearSomething
                    //investigatesound
                    //if seesplayer
                    //RunFATBOYRUN
                    return;
                }
            case TypeofEnemy.Retracing:
                {
                    TurningAndMovingAI();
                    Retrace();
                    if (sensing.CanSee(GameManager.instance.player1))
                    {
                        playerCharacter_ = GameManager.instance.player1.transform;
                        EnemyType = SwitchTo;
                    }
                    if (sensing.CanHear(GameManager.instance.player1.gameObject))
                    {
                        EnemyType = TypeofEnemy.sawPlayer;
                    }
                    //check transitions
                    //if hearSomething
                    //investigatesound
                    //if seesplayer
                    //RunFATBOYRUN
                    return;
                }
            case TypeofEnemy.Lookout:
                {
                    LookoutAI();
                    if (sensing.CanSee(GameManager.instance.player1))
                    {
                        playerCharacter_ = GameManager.instance.player1.transform;
                        EnemyType = SwitchTo;
                    }
                    if (sensing.CanHear(GameManager.instance.player1.gameObject))
                    {
                        EnemyType = TypeofEnemy.sawPlayer;
                    }
                    //check transitions
                    //if hearSomething
                    //investigatesound
                    //if seesplayer
                    //FoundYou
                    return;
                }
            case TypeofEnemy.Flee:
                {
                    RunFATBOYRun();
                    return;
                }
            case TypeofEnemy.sawPlayer:
                {
                    FoundYou();
                    return;
                }
            default:
                {
                    Debug.Log("No/Invalid State Was Selected");
                    break;
                }
        }
    }

    IEnumerator FiringTime()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            fireTimer_++;
        }
    }

    IEnumerator SearchTime()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            searchTimer_++;
        }
    }


    //halt function if AI has to stop
    void Idle()
    {
        //do nothing and just watch
    }

    //loop function for if the AI just keeps going "in circles"
    void Looping()
    {
        if (Vector3.SqrMagnitude(waypointSystem[currentWaypoint_].position - tankTransform_.position) < (howClose * howClose))
        {
            if (currentWaypoint_ < waypointSystem.Length - 1) //keep going until the end of list is reached
            {
                currentWaypoint_++;
            }
            else //if reached the end of waypoint then go back to beggining of list
            {
                currentWaypoint_ = 0;
                //Debug.Log("Looping");
            }
        }
    }

    //retrace function if going back over patrol route
    void Retrace()
    {
        if (Vector3.SqrMagnitude(waypointSystem[currentWaypoint_].position - tankTransform_.position) < (howClose * howClose))
        {
            if (!endOfPatrol_) //if hasn't ended patrol go to next waypoint
            {
                if (currentWaypoint_ < waypointSystem.Length - 1) //keep going until the end of list is reached
                {
                    currentWaypoint_++;
                }
                else //if reached the end of patrol set true and start headding back
                {
                    endOfPatrol_ = true;
                    //Debug.Log("Retrace");
                }
            }

            else
            {
                if (currentWaypoint_ > 0) //keep going until the the beginning is reached again
                {
                    currentWaypoint_--;
                }
                else //if reached the beginning restart patrol
                {
                    endOfPatrol_ = false;
                    //Debug.Log("Retrace");
                }
            }
        }
    }

    void LookoutAI()
    {
        if (!AITurned(waypointSystem[currentWaypoint_].position, speedInfo_.turnSpeed))
        {
            //Do nothing if not turned towards next location
        }

        if (AITurned(waypointSystem[currentWaypoint_].position, speedInfo_.turnSpeed))
        {
            if (currentWaypoint_ < waypointSystem.Length - 1) //keep going until the end of list is reached
            {
                currentWaypoint_++;
            }
            else //once end of list is reached halt movement
            {
                currentWaypoint_ = 0;
                //Debug.Log("Looking");
            }
        }
    }

    //flee function for if player is seen
    void RunFATBOYRun()
    {
        StateCheck();
        vectorFlee_ = -1 * (playerCharacter_.transform.position - tankTransform_.position);
        vectorFlee_.Normalize();
        vectorFlee_ *= fleeDistance;

        fleePosition_ = vectorFlee_ + tankTransform_.position;

        AITurned(fleePosition_, speedInfo_.turnSpeed);
        motor_.Move(speedInfo_.moveSpeed);
        //Debug.Log("Running");
        
    }

    //chase function for if player is found
    void FoundYou()
    {
        AITurned(playerCharacter_.transform.position, speedInfo_.turnSpeed);
        motor_.Move(speedInfo_.moveSpeed);
        StateCheck();
        //Debug.Log("Found");
    }

    void StateCheck() //checks time against developer set time to either stop searching or continue (MIGHT add in last position check)
                      //assuming enemy DOESN'T have player in field of view anymore
    {
        if (sensing.CanSee(GameManager.instance.player1.gameObject))
        {
            if (!searchTimeStarted_)
            {
                StartCoroutine("SearchTime");
                searchTimeStarted_ = true;
            }
            searchTimer_ = 0;
        }
        else if (!sensing.CanSee(GameManager.instance.player1.gameObject))
        {
            if ((EnemyType == TypeofEnemy.Flee && (searchTimeAllowed + 10) <= searchTimer_) ||
                 EnemyType != TypeofEnemy.Flee && searchTimeAllowed <= searchTimer_)
                 //EnemyType check is to see if enemy was selected as flee or otherwise. if fleeing add an additional 10 seconds to count for otherwise
                 //leave alone
            {
                searchTimeStarted_ = false;
                playerCharacter_ = null;
                StopCoroutine("SearchTime");
                searchTimer_ = 0;
                EnemyType = originalState_;

            }
        }
    }

    void TurningAndMovingAI() //allows turning to move towards next waypoint
    {
        if (!AITurned(waypointSystem[currentWaypoint_].position, speedInfo_.turnSpeed)) //checks to see if AI has turned or not
        {
            //Do nothing if not turned towards next location
        }
        else
        {
            //moves forward
            motor_.Move(speedInfo_.moveSpeed);
        }
    }


    public bool AITurned(Vector3 target, float speed) //if AI hasn't turned to next waypoint then continue turning (something similar for firing)
    {
        //sets up the rotation target here
        Vector3 targetVector = target - tankTransform_.position;
        Quaternion rotationTarget = Quaternion.LookRotation(targetVector);

        if (rotationTarget == tankTransform_.rotation) //if AI has turned set true and enable movement
        {
            return true;
        }

        else //if AI HASN'T finished turning then keep turning
        {
            //turn towards target location but only move at turn speed
            tankTransform_.rotation = Quaternion.RotateTowards(tankTransform_.rotation, rotationTarget, speed * Time.deltaTime);
            return false;
        }
    }
}

